<section class="hero">

    <div class="slides"> 
        <?php
        $args = array( 'meta_key' => 'lci_hero', 'meta_value' => 'on', 'posts_per_page' => 10 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); ?>

            <article class="hero-slide">
                <figure class="hero-image">
                    <?php the_post_thumbnail("hero") ?>
                </figure>

                <div class="hero-slide-content">
                    <h1 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h1>
                    <p class="hero-content-excerpt"><?php echo excerpt(15); ?></p>
                </div>
            </article>

        <?php 
        endwhile;
        ?>
    </div>
</section>
