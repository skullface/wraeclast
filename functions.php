<?php
/**
 * Wraeclast functions and definitions
 *
 * @package Wraeclast
 */

if ( ! function_exists( 'wraeclast_setup' ) ) :
function wraeclast_setup() {

	/**
	 * Make theme available for translation
	 */
	load_theme_textdomain( 'wraeclast', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary_navigation' => __( 'Primary Menu', 'wraeclast' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'gallery' ) );

}
endif; // wraeclast_setup
add_action( 'after_setup_theme', 'wraeclast_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function wraeclast_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wraeclast' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'wraeclast_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function wraeclast_scripts() {
	wp_enqueue_style( 'wraeclast-style', get_stylesheet_uri() );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'wraeclast_js', get_template_directory_uri() . '/js/main.js' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wraeclast_scripts' );

/**
 * No fucking height/width on images
 */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 50 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom navigation menu walker.
 */
require get_template_directory() . '/inc/nav.php';

/**
 * Custom shortcode functions.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Let's make the wp_head a lot nicer and less Wordpress-y
 */
function roots_head_cleanup() {
  remove_action('wp_head', 'feed_links', 2);
  remove_action('wp_head', 'feed_links_extra', 3);
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

  global $wp_widget_factory;
  remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));

  add_filter('use_default_gallery_style', '__return_null');
}
add_action('init', 'roots_head_cleanup');

/**
 * Custom hero image size
 */

if ( function_exists( 'add_image_size' ) ) { 
  add_image_size( 'hero', 600, 310, true ); //(cropped)
  add_image_size( 'home', 600, 310, true ); //(cropped)
}

/**
 * Hero metabox
 */
$meta_box = array(
    'id' => 'hero',
    'title' => 'Featured Post?',
    'page' => 'post',
    'context' => 'advanced',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Make this a featured post on the homepage?',
            'id' => 'lci_hero',
            'type' => 'checkbox'
        )
    )
);
add_action('admin_menu', 'wraeclast_add_box');

// Add meta box
function wraeclast_add_box() {
    global $meta_box;

    add_meta_box($meta_box['id'], $meta_box['title'], 'wraeclast_show_box', $meta_box['page'], $meta_box['context'], $meta_box['priority']);
}

// Callback function to show fields in meta box
function wraeclast_show_box() {
    global $meta_box, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="wraeclast_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    foreach ($meta_box['fields'] as $field) {
        // get current post meta data
        $meta = get_post_meta($post->ID, $field['id'], true);

        echo '<tr>',
                '<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
                '<td>';
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
        echo     '<td>',
            '</tr>';
    }

    echo '</table>';
}

add_action('save_post', 'wraeclast_save_data');

// Save data from meta box
function wraeclast_save_data($post_id) {
    global $meta_box;

    // verify nonce
    if (!wp_verify_nonce($_POST['wraeclast_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($meta_box['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

/**
 * Excerpts
 */

function new_excerpt_more( $more ) {
	return '&hellip; <a href="'. get_permalink( get_the_ID() ) . '" class="read-more">Read more</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function new_excerpt_length( $length ) {
	return 45;
}
add_filter( 'excerpt_length', 'new_excerpt_length', 999 );

/**
 * Hero excerpt
 */

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'&hellip; <a href="'. get_permalink( get_the_ID() ) . '" class="read-more">Read more</a>';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'&hellip; <a href="'. get_permalink( get_the_ID() ) . '" class="read-more">Read more</a>';
  } else {
    $content = implode(" ",$content);
  }	
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}