<?php
/**
 * @package Wraeclast
 */
?>

<li <?php post_class(); ?>>

	<span class="post-archive-category">
		<?php the_category(' '); ?>
	</span>

	<a href="<?php the_permalink(); ?>" rel="bookmark" class="post-archive-entry-title"><?php the_title(); ?></a>

    <figure class="post-archive-thumbnail">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail('thumb'); ?>
		</a>
    </figure>

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div>

</li>