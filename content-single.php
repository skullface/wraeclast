<?php
/**
 * @package Wraeclast
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<div class="entry-meta">
		<?php the_category(' '); ?>
		<time datetime="<?php the_date('Y-m-d');?>"><?php the_time('F j, Y');?></time>
		<a href="<?php the_permalink(); ?>#comments" class="link-to-comments"><?php comments_number('No Comments', '1 Comment', '% Comments'); ?></a>
	</div>
	<div class="entry-content">
		<?php the_content(); ?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wraeclast' ),
				'after'  => '</div>',
			) );
		?>
	</div>
</article>