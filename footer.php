<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Wraeclast
 */
?>

	</div><!-- #content -->
<footer class="site-footer" role="contentinfo">
	<div id="secondary" class="content-area widget-area" role="complementary">

		<aside class="widget">
			<h3>Elsewhere</h3>
			<nav class="navigation">
			<ul class="menu-social">
				<li>
					<a href="<?php bloginfo('url'); ?>/category/games/" class="twitter">@leftclick_it</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/category/gear/" class="facebook">/leftclick_it</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/category/interviews/" class="twitch">leftclick_it</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/about/" class="email">info@leftclick.it</a>
				</li>
			</ul>
		</nav>
		</aside>

		<aside class="widget">

		<h3>Games</h3>
		<nav class="navigation">
			<ul class="menu-main-small">
				<li>
					<a href="<?php bloginfo('url'); ?>/category/cs-go">CS:GO</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/category/diablo-iii">Diablo III</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/category/dota-2">Dota 2</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/category/starcraft-ii">StarCraft II</a>
				</li>
		</nav>
		</aside>

		<aside class="widget widget-internal">


		<nav class="navigation">
		<?php get_search_form(); ?>

			<ul class="menu-internal">
				<li>
					<a href="<?php bloginfo('url'); ?>/about">About</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/privacy-tou">Privacy, Terms &amp; Conditions</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/sponsorship">Sponsorship</a>
				</li>
				<li>
					<a href="<?php bloginfo('url'); ?>/feed">RSS</a>
				</li>
				<li>
					<a href="#top" class="back-to-top">&#61656;</a>
				</li>
		</nav>

		<!--
		-->

		<p>&copy; 2013 leftclick.it. All rights reserved.</p>
		</aside>

</footer>

</div><!-- #page -->
<script>
	new UISearch( document.getElementById( 'sb-search' ) );

    jQuery(window).load(function(){
      jQuery('.hero').flexslider({
        animation: "fade"
      });
    });
</script>

<?php wp_footer(); ?>

</body>
</html>