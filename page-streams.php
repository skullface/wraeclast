<?php
/**
 * Template: About
 */

get_header(); ?>

	<noscript>
		<style>
			.st-accordion ul li{
				height:auto;
			}
			.st-accordion ul li > a span{
				visibility:hidden;
			}
		</style>
	</noscript>

    <script type="text/javascript">
        jQuery(function() {
		
			jQuery('#streams-js').accordion({
				oneOpenedItem	: true
			});
			
        });
    </script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div id="page-content post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->

				<div class="entry-content">
				<div id="streams-js" class="st-accordion">
					<ul>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; // end of the loop. ?>
						

					</ul>
				</div>
				</div>
			</div>


		</main><!-- #main -->
	</div><!-- #primary -->



<?php get_sidebar(); ?>
<?php get_footer(); ?>
