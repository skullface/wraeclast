<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<link rel="shortcut icon" href="http://leftclick.it/wp-content/themes/sanctuary/images/favico.png" type="image/x-icon">

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" media="all"></script>
	<![endif]-->

    <!-- Twitter card information -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@leftclick_it">
    <meta name="twitter:creator" content="@roccocostello">

    <!-- Google(+) information -->
    <link rel="author" href="https://plus.google.com/pizza">
    <meta name="author" content="Rocco Costello">
    <link rel="canonical" href="http://leftclick.it">
    <meta name="description" content="">

    <!-- Typekit -->
	<script>
	  (function() {
	    var config = {
	      kitId: 'hjs4add',
	      scriptTimeout: 3000
	    };
	    var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
	  })();
	</script>

	<?php if ( is_home() ) : ?>
	<script>
	    jQuery(window).load(function(){
	      jQuery('.hero').flexslider({
	        animation: "slide"
	      });
	    });
	</script>
	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="leftclickit">
	<header class="site-header" role="banner">
		<div class="content-area">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'walker' => new MV_Cleaner_Walker_Nav_Menu(), 'container' => false, 'menu_class' => 'site-navigation-menu' ) ); ?>
			</nav>

			<div id="sb-search" class="sb-search">
				<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<input type="search" class="sb-search-input" placeholder="<?php echo esc_attr_x( 'Search&hellip;', 'placeholder', 'wraeclast' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'wraeclast' ); ?>">
					<input type="submit" class="sb-search-submit" value="<?php echo esc_attr_x( '', 'submit button', 'wraeclast' ); ?>">
					<span class="sb-icon-search"></span>
				</form>
			</div>

		</div>
	</header>


<?php if ( is_home() && !get_option('ss_disable') ) get_template_part('hero'); ?>
