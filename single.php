<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Wraeclast
 */

get_header(); ?>

<!--
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
	<div id="content" class="site-content">
		<div class="post-background-container" style="background-image: url('<?php echo $image[0]; ?>')"></div>
<?php else : ?>
	<div id="content" class="site-content">
<?php endif; ?>
-->
	<div id="content" class="site-content">
	<div id="primary" class="content-area">

		<?php while ( have_posts() ) : the_post(); ?>
		<main id="main" class="site-main" role="main">

			<?php get_template_part( 'content', 'single' ); ?>

			<?php wraeclast_author_box(); ?>

			<?php wraeclast_content_nav( 'nav-below' ); ?>
		</main>

	</div>
	</div>

	<div class="comments-section">
		<?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() )
				comments_template();
		?>

		<?php endwhile; // end of the loop. ?>
	</div>

	<section class="recent-posts">
		<ul class="related-posts content-area">
			<?php
				$recent_posts = new WP_Query( $args );
				$recent_posts->query('showposts=4');

				if($recent_posts->have_posts()) :
				    while ( $recent_posts->have_posts() ) :
				    $recent_posts->the_post();
			?>
				<li><a href="<?php the_permalink() ?>">
					<span class="related-post-category">
				        <?php 
				        	foreach((get_the_category()) as $category) {
				        		echo $category->cat_name . ' ';
				        	}
				        ?>
				    </span>
				    <span class="related-post-thumbnail">
				       <?php the_post_thumbnail('thumbnail'); ?>
				    </span>
				    <span class="recent-post-title">
				    	<?php the_title(); ?>
				    </span>
				</a>
				</li>

			<?php
				endwhile; 
				else:
			?>
			
			<?php
				endif;
				wp_reset_postdata();
			?>
		</ul>
	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>