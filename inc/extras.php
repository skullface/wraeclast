<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Wraeclast
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function wraeclast_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'wraeclast_page_menu_args' );

/**
 * Adds custom classes to the array of body classes.
 */
function wraeclast_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	return $classes;
}
add_filter( 'body_class', 'wraeclast_body_classes' );

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 */
function wraeclast_wp_title( $title, $sep ) {
	global $page, $paged;

	if ( is_feed() )
		return $title;

	// Add the blog name
	$title .= get_bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .= " $sep $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		$title .= " $sep " . sprintf( __( 'Page %s', 'wraeclast' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'wraeclast_wp_title', 10, 2 );

/**
 * Remove widows
 * filter the_title() to remove any chance of a typographic widow
 * http://imgiseverything.co.uk/articles/removing-typographic-widows-in-wordpress-page-titles/
 */

function remove_widows($title){

    $title_length = strlen($title);
     
    if(strpos($title, 'a href=') > 0){
        // this is a link so
        // work out where the anchor text starts and ends
        $start_of_text = strpos($link, '">');
        $end_of_text = strpos($link, '</a>');
        $end_of_text = ($title_length -  $end_of_text);
        $anchor_text = substr($title, $start_of_text, $end_of_text);        
    } else{
        $start_of_text = 0;
        $end_of_text = $title_length;
        $anchor_text = $title;
    }
    // convert the title into an array of words
    $anchor_array = explode(' ', $anchor_text);
     
    // Provided there's multiple words in the anchor text
    // then join all words (except the last two) together by a space.
    // Join the last two with an &nbsp; which is where the
    // magic happens
    if(sizeof($anchor_array) > 1){
        $last_word = array_pop($anchor_array);
        $title_new = join(' ', $anchor_array) . '&nbsp;' . $last_word;
        $title = substr_replace($title, $title_new, $start_of_text, $end_of_text);
    }
    return $title;
     
}
add_filter('the_title', 'remove_widows');

/*
 * Add custom social fields to user profile
 */
function wraeclast_contact_methods( $contactmethods ) {
 
    $contactmethods[ 'twitter' ] = 'Twitter username';
    $contactmethods[ 'twitch' ] = 'Twitch username';
    $contactmethods[ 'youtube' ] = 'YouTube username';
 
    return $contactmethods;
}
 
add_filter( 'user_contactmethods', 'wraeclast_contact_methods' );

/*
 * Remove links from images
 */

/* =SET NO LINK ON IMAGE INSERT AS DEFAULT
-------------------------------------------------------------- */
$options = get_option( 'remove-image-links-options' );
update_option( 'image_default_link_type', $options['default_link'] );


/* =REMOVE LINK FROM IMAGES IN POSTS
-------------------------------------------------------------- */
function shs_remove_links( $content ) {
    // get options
    $options = get_option( 'remove-image-links-options' );
    
    // make sure they exist
    if( !$options ) {
        $options = array(
            'default_link' => 'none',
            'remove_jpg' => 1,
            'remove_jpeg' => 1,
            'remove_gif' => 1,
            'remove_png' => 1,
            'remove_bmp' => 1
        );
        
        // add default options if they don't
        add_option( 'remove-image-links-options', $options, '', 'no' );
    }
    
    // regex pattern of links that should be removed
    $pattern = array();
    if( $options['remove_jpg'] ) array_push( $pattern, '/<a(.*?)href="(.*?).jpg"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
    if( $options['remove_jpeg'] ) array_push( $pattern, '/<a(.*?)href="(.*?).jpeg"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
    if( $options['remove_gif'] ) array_push( $pattern,  '/<a(.*?)href="(.*?).gif"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
    if( $options['remove_png'] ) array_push( $pattern,  '/<a(.*?)href="(.*?).png"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
    if( $options['remove_bmp'] ) array_push( $pattern,  '/<a(.*?)href="(.*?).bmp"(.*?)>(.*?)<img(.*?)>(.*?)<\/a>/' );
    
    // parse content through regex and return
    $result = preg_replace( $pattern, '$4<img$5>$6', $content );
    return $result;
}

add_filter( 'the_content', 'shs_remove_links' ); // cull image links