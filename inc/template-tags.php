<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Wraeclast
 */

if ( ! function_exists( 'wraeclast_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 */
function wraeclast_content_nav( $nav_id ) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'post-navigation' : 'paging-navigation';

	?>
	<nav role="navigation" id="post-navigation" class="<?php echo $nav_class; ?>">
		<?php if ( is_single() ) : // navigation links for single posts ?>

			<?php previous_post_link( '<div class="nav-previous"><span>Previous article</span> %link</div>' ); ?>
			<?php next_post_link( '<div class="nav-next"><span>Next article</span> %link</div>' ); ?>

		<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<div class="nav-previous"><span>Older posts</span></div>', 'wraeclast' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( '<div class="nav-next"><span>Newer posts</span></div>', 'wraeclast' ) ); ?></div>
			<?php endif; ?>

	<?php endif; ?>
	</nav>
	<?php
}
endif; // wraeclast_content_nav


/**
 * Related posts
 */
function my_related_posts() {
$args = array(
        'posts_per_page' => 5,
        'post_in'  => the_tags()
    );
echo '<ul>';
while ( $the_query->have_posts() ) : $the_query->the_post();
        ?> 
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </li>
        <?php
endwhile;
echo '</ul>';
 wp_reset_postdata();
}

if ( ! function_exists( 'wraeclast_comment' ) ) :

/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function wraeclast_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php _e( 'Pingback:', 'wraeclast' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( 'Edit', 'wraeclast' ), '<span class="edit-link">', '</span>' ); ?>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
		<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
			<header class="comment-meta">

				<div class="comment-avatar">
					<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
				</div>

				<div class="comment-metadata">
					<?php printf( __( '%s', 'wraeclast' ), sprintf( '<span class="comment-author">%s</span>', get_comment_author_link() ) ); ?>

					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>" class="comment-timestamp">
						<time datetime="<?php comment_time( 'c' ); ?>">
							<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'wraeclast' ), get_comment_date(), get_comment_time() ); ?>
						</time>
					</a>
						
					<?php
						comment_reply_link( array_merge( $args, array(
							'add_below' => 'div-comment',
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
							'before'    => '<span class="comment-reply">',
							'after'     => '</span>',
						) ) );
					?>
				</div>

				<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'wraeclast' ); ?></p>
				<?php endif; ?>
			</header>

			<div class="comment-content">
				<?php comment_text(); ?>

			</div>

		</article>

	<?php
	endif;
}
endif; // ends check for wraeclast_comment()

/**
 * Comment form
 */
function wraeclast_comment_form($arg) {
    $arg['author'] = '<div class="comment-form-fields"><p class="comment-form-author"><label for="author">Name</label>' . 
    '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
    '"' . $aria_req . ' /></p>';
    $arg['email'] = '<p class="comment-form-email"><label for="email">Email <span class="form-field-note">No spam, no shady stuff.</span></label>' .
    '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
    '" ' . $aria_req . ' /></p>';
    $arg['url'] = '<p class="comment-form-url"><label for="url">Website <span class="form-field-note">Optional</span></label>' .
    '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
    '" /></p></div>';
    return $arg;
}
add_filter('comment_form_default_fields', 'wraeclast_comment_form');


/**
 * Author avatar URL
 */
function get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
}

/**
 * Author meta info markup to echo~
 */
function wraeclast_author_box() {
?>
	<div id="entry-author-meta">
		<div class="entry-author-meta-info">
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><img src="<?php echo get_avatar_url(get_avatar( $curauth->ID, 100 )); ?>" class="author-avatar"></a>

			<strong><?php the_author(); ?></strong>
			<?php the_author_meta('description'); ?>

			<?php
    $twitter = get_post_meta($post->ID, 'twitter', true); 
    $twitch = get_post_meta($post->ID, 'twitch', true); 
    $youtube = get_post_meta($post->ID, 'youtube', true); 

	if ($twitter) {
	    echo "<p><a href='http://twitter.com/$twitter'>$twitter</a></p>";
	}
	?>
		</div>
		<div class="entry-author-meta-links">
			<ul>
				<li><a class="plus" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">View all posts by <?php the_author_meta('first_name'); ?></a></li>
				<li><a class="twitter" href="http://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on Twitter">@<?php the_author_meta('twitter'); ?></a></li>
				<li><a class="twitch" href="http://twitch.tv/<?php the_author_meta( 'twitch' ); ?>" title="Follow <?php the_author_meta( 'display_name' ); ?> on Twitch">@<?php the_author_meta('twitch'); ?></a></li>
			</ul>
		</div>
	</div>
<?php
}

/**
 * Don't auto-width images with captions, add HTML5 figure + figcaption markup support
 */
add_filter('img_caption_shortcode', 'html5_img_caption_shortcode', 1, 3 );

function html5_img_caption_shortcode($string, $attr, $content = null) {
    extract(shortcode_atts(array(
        'id'    => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => ''
    ), $attr)
    );
    if ( 1 > (int) $width || empty($caption) )
        return $content;
    if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
 
    return '<figure ' . $id . 'class="caption ' . esc_attr($align) . '">' .
        do_shortcode( $content ) .
        '<figcaption class="caption-text">' . $caption . '</figcaption>'.
        '</figure>';
}

/**
 * Returns true if a blog has more than 1 category
 */
function wraeclast_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so wraeclast_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so wraeclast_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in wraeclast_categorized_blog
 */
function wraeclast_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'wraeclast_category_transient_flusher' );
add_action( 'save_post',     'wraeclast_category_transient_flusher' );