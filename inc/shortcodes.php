<?php
/**
 * all the custom shortcodes, all the custom shortcodes
 */

// Stream Accordions
function stream_accordion($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '#',
		'twitch_username' => '#',
	), $atts));
return '<li><a href="#">' . $title . '</a><div class="st-content"><div class="video-wrapper"><object id="live_embed_player_flash" width="920" height="540" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="flashvars" value="hostname=sv.twitch.tv&amp;channel=' . $twitch_username . '&amp;auto_play=true&amp;start_volume=25" /><param name="src" value="http://sv.twitch.tv/widgets/live_embed_player.swf?channel=' . $twitch_username . '" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><embed id="live_embed_player_flash" width="920" height="540" type="application/x-shockwave-flash" src="http://sv.twitch.tv/widgets/live_embed_player.swf?channel=' . $twitch_username . '" allowFullScreen="true" allowScriptAccess="always" allowNetworking="all" flashvars="hostname=sv.twitch.tv&amp;channel=' . $twitch_username . '&amp;auto_play=true&amp;start_volume=25" allowfullscreen="true" allowscriptaccess="always" allownetworking="all" bgcolor="#000000" /></object></div></div></li>';
}
add_shortcode('stream', 'stream_accordion');

?>

